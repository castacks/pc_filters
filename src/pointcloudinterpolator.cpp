/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/



#include <pc_filters/pointcloudinterpolator.h>

using namespace CA;


TimeVector getTimeVector(const OffsetStruct &st, unsigned char * data)
{

  TimeVector tv;
  if(st.tFloat)
    tv.time = *(reinterpret_cast<float*> ((&data[st.tOff])));
  else
    tv.time = *(reinterpret_cast<double*> (&data[st.tOff]));
  if(st.xFloat)
    tv.point[0] = *(reinterpret_cast<float*> (&data[st.xOff]));
  else
    tv.point[0] =  *(reinterpret_cast<double*> (&data[st.xOff]));
  if(st.yFloat)
    tv.point[1] = *( reinterpret_cast<float*> (&data[st.yOff]));
  else
    tv.point[1] = *(reinterpret_cast<double*> (&data[st.yOff]));
   if(st.zFloat)
     tv.point[2] = *( reinterpret_cast<float*> (&data[st.zOff]));
  else
    tv.point[2] = *(reinterpret_cast<double*> (&data[st.zOff]));
   return tv;
}

void setVector(TimeVector &tv, const OffsetStruct &st, unsigned char *data)
{
  if(st.tFloat)
    {
      float t = tv.time;
      memcpy (&data[st.tOff], &t, sizeof (float));
    }
  else
    {
      memcpy (&data[st.tOff], &tv.time, sizeof (double));
    }
  if(st.xFloat)
    {
      float t = tv.point[0];
      memcpy (&data[st.xOff], &t, sizeof (float));
    }
  else
    {
      memcpy (&data[st.xOff], &tv.point[0], sizeof (double));
    }
  if(st.yFloat)
    {
      float t = tv.point[1];
      memcpy (&data[st.yOff], &t, sizeof (float));
    }
  else
    {
      memcpy (&data[st.yOff], &tv.point[1], sizeof (double));
    }
  if(st.zFloat)
    {
      float t = tv.point[2];
      memcpy (&data[st.zOff], &t, sizeof (float));
    }
  else
    {
      memcpy (&data[st.zOff], &tv.point[2], sizeof (double));
    }
  
}

bool PointCloudInterpolator::transform(const std::string &targetFrame, sensor_msgs::PointCloud2 &scan,tf::Transformer &tf, int tfLookupincrement)
{

  //If the frames match don't transform
  if(targetFrame == scan.header.frame_id)
    return true;
  //First lookup the indices and data sizes for x, y, z.
 int  xInd = -1,yInd =-1,zInd = -1, tInd=-1;
  OffsetStruct os;

  for(unsigned int i= 0;i<scan.fields.size();i++)
    {
      if(scan.fields[i].name == "x")
	{
	  xInd = i;
	  os.xOff = scan.fields[i].offset;
	  if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
	    os.xFloat = true;
	  else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
	    os.xFloat = false;
	  else
	    {
	      ROS_ERROR_STREAM("Field was of wrong type");
	      return false;
	    }
	}
      else if(scan.fields[i].name == "y")
	{
	  yInd = i;
	  os.yOff = scan.fields[i].offset;
	  if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
	    os.yFloat = true;
	  else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
	    os.yFloat = false;
	  else
	    {
	      ROS_ERROR_STREAM("Field was of wrong type");
	      return false;
	    }

	}
      else if(scan.fields[i].name == "z")
	{
	  zInd = i;
	  os.zOff = scan.fields[i].offset;
	  if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
	    os.zFloat = true;
	  else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
	    os.zFloat = false;
	  else
	    {
	      ROS_ERROR_STREAM("Field was of wrong type");
	      return false;
	    }

	}
      else if(scan.fields[i].name == "stamps")
	{
	  tInd = i;
	  os.tOff = scan.fields[i].offset;
	  if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
	    {
	      os.tFloat = true;
	      ROS_WARN_STREAM("Time stamp field probably too small");
	    }
	  else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
	    os.tFloat = false;
	  else
	    {
	      ROS_ERROR_STREAM("Field was of wrong type");
	      return false;
	    }

	}
    }

  if(xInd == -1 || yInd == -1 || zInd == -1 || tInd == -1)
    {
      ROS_ERROR_STREAM("Was not able to find all the required fields to transform the point cloud. Need x,y,z,stamps");
      return false;
    }
  //Now that we have the fields transform the points
  int numPoints = std::min((int32_t) scan.width * scan.height, (int32_t) scan.data.size()/ scan.point_step) ;
  if(numPoints==0)
    return true;
  TimeVector point;
  int transformStartI=0;
  int transformEndI = std::min(numPoints-1,transformStartI + tfLookupincrement);
  tf::StampedTransform start_transform;
  tf::StampedTransform end_transform;
  tf::StampedTransform cur_transform;
  
  point = getTimeVector(os,&scan.data[transformStartI * scan.point_step]);
  double start_time = point.time;
  point = getTimeVector(os,&scan.data[transformEndI * scan.point_step]);
  double end_time = point.time;
  point = getTimeVector(os,&scan.data[(numPoints-1) * scan.point_step]);
  double total_end_time = point.time;
  int checkcount=0;
  while( !tf.canTransform(targetFrame, scan.header.frame_id, ros::Time(start_time))|| !tf.canTransform(targetFrame, scan.header.frame_id, ros::Time(total_end_time)))
    {
      ros::Duration(0.1).sleep();
      checkcount++;
      if(checkcount>3)
	{
	  ROS_ERROR_STREAM("Was not able to do an interpolated transform from pointcloud2 "<<scan.header.frame_id <<" -> "<<targetFrame);
	  return false;
	}
    }



  tf.lookupTransform(targetFrame, scan.header.frame_id, ros::Time(start_time), start_transform);
  tf.lookupTransform(targetFrame, scan.header.frame_id, ros::Time(end_time), end_transform);

  double range = end_time - start_time ;

  for(int i=0;i< numPoints;++i)
    {
      if(i>=transformEndI)
	{
	  transformStartI= i;start_time = end_time;
	  transformEndI = std::min(numPoints-1,transformStartI + tfLookupincrement);
	  point = getTimeVector(os,&scan.data[transformEndI * scan.point_step]);
	  end_time = point.time; 
	  start_transform = end_transform;
	  tf.lookupTransform(targetFrame, scan.header.frame_id, ros::Time(end_time), end_transform);
	  range = std::max(1e-14,end_time - start_time);
	}
      point = getTimeVector(os,&scan.data[i * scan.point_step]);
      //Now interpolate between the transforms
      
      double ratio = (point.time - start_time) / range;
      
      tf::Vector3 v;
      v = start_transform.getOrigin() *(1.0 - ratio) + ratio * end_transform.getOrigin();

      cur_transform.setOrigin(v) ;
      tf::Quaternion q1, q2 ;
      start_transform.getBasis().getRotation(q1) ;
      end_transform.getBasis().getRotation(q2) ;
      cur_transform.setRotation( slerp( q1, q2 , ratio) ) ;
            
      tf::Vector3 pointOut = cur_transform * point.point ;
      point.point =pointOut;
      setVector(point,os,&scan.data[i * scan.point_step]);     
    }
  scan.header.frame_id = targetFrame;
  return true;
}  
  
std::vector<PointD> PointCloudInterpolator::getPointsfromPointCloud2(sensor_msgs::PointCloud2 &scan)
{

    std::vector<PointD>  points;

    OffsetStruct os = getXYZoffset(scan);
    
    unsigned int num_points = scan.height*scan.width;
    void *arr  = calloc(1,8);
    double value;
    PointD p;
    
    if(os.xOff == -1 || os.yOff == -1 || os.zOff == -1 )
    {
        return points;
    }
    
    for(unsigned int i=0; i<num_points;i++)
    {
        if(os.xFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.xOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.xOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p.x = value;
        
        
        if(os.yFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.yOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.yOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p.y = value;
        
        
        if(os.zFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.zOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.zOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p.z = value;
        
        points.push_back(p);
    }

    free(arr);
    return points;

};

std::vector<Vector4D>  PointCloudInterpolator::getPoints4fromPointCloud2(sensor_msgs::PointCloud2 &scan)
{
    std::vector<Vector4D>  points;

    OffsetStruct os = getXYZIoffset(scan);

    unsigned int num_points = scan.height*scan.width;
    void *arr  = calloc(1,8);
    double value;
    Vector4D p;

    if(os.xOff == -1 || os.yOff == -1 || os.zOff == -1 || os.iOff == -1)
    {
        return points;
    }

    for(unsigned int i=0; i<num_points;i++)
    {
        if(os.xFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.xOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.xOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p[0] = value;


        if(os.yFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.yOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.yOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p[1] = value;


        if(os.zFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.zOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.zOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p[2] = value;

        if(os.iFloat)
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.iOff],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + os.iOff],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        p[3] = value;
        points.push_back(p);
    }

    free(arr);
    return points;

};

OffsetStruct PointCloudInterpolator::getXYZoffset(sensor_msgs::PointCloud2 &scan)
{
    OffsetStruct os;

    for(unsigned int i= 0;i<scan.fields.size();i++)
    {
        if(scan.fields[i].name == "x")
        {
          os.xOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.xFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.xFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.xOff = -1;
              return os;
            }
        }
        else if(scan.fields[i].name == "y")
        {
          os.yOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.yFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.yFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.yOff = -1;
              return os;
            }

        }
        else if(scan.fields[i].name == "z")
        {
          os.zOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.zFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.zFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.zOff = -1;
              return os;
            }

        }
    }
    
    return os;
};

OffsetStruct PointCloudInterpolator::getXYZIoffset(sensor_msgs::PointCloud2 &scan)
{
    OffsetStruct os;

    for(unsigned int i= 0;i<scan.fields.size();i++)
    {
        if(scan.fields[i].name == "x")
        {
          os.xOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.xFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.xFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.xOff = -1;
              return os;
            }
        }
        else if(scan.fields[i].name == "y")
        {
          os.yOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.yFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.yFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.yOff = -1;
              return os;
            }

        }
        else if(scan.fields[i].name == "z")
        {
          os.zOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.zFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.zFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.zOff = -1;
              return os;
            }

        }
        else if(scan.fields[i].name == "echo_pw")
        {
          os.iOff = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            os.iFloat = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            os.iFloat = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              os.iOff = -1;
              return os;
            }

        }
    }

    return os;
};



void PointCloudInterpolator::transformPointDVector(std::vector<PointD> &points,tf::Transform &transform)
{
    Vector3D p;
  	p.x() = transform.getOrigin().x(); p.y() = transform.getOrigin().y(); p.z() = transform.getOrigin().z();
  	Eigen::Quaterniond q;
  	q.x() = transform.getRotation().x(); q.y() = transform.getRotation().y(); q.z() = transform.getRotation().z(); q.w() = transform.getRotation().w();
  	
  	Vector3D p_;
  	
  	
  	for(unsigned int i=0;i<points.size();i++)
  	{
  	    p_.x()= points[i].x ;p_.y()= points[i].y ;p_.z()= points[i].z;
  	    p_ = q*(p_) + p;
  	    if(std::isnan(p_.x())||std::isnan(p_.y())||std::isnan(p_.z()))
  	    {
  	        ROS_ERROR("NaNs");
  	    }
  	    else
  	    {
  	        points[i].x = p_.x(); points[i].y = p_.y(); points[i].z = p_.z();
        }
  	    
  	}      	
}



void PointCloudInterpolator::transformPointVector(std::vector<geometry_msgs::Point> &points,tf::Transform &transform)
{
    Vector3D p;
  	p.x() = transform.getOrigin().x(); p.y() = transform.getOrigin().y(); p.z() = transform.getOrigin().z();
  	Eigen::Quaterniond q;
  	q.x() = transform.getRotation().x(); q.y() = transform.getRotation().y(); q.z() = transform.getRotation().z(); q.w() = transform.getRotation().w();
  	
  	Vector3D p_;
  	
  	for(unsigned int i=0;i<points.size();i++)
  	{
  	    p_.x()= points[i].x ;p_.y()= points[i].y ;p_.z()= points[i].z;
  	    p_ = q*(p_) + p;
  	    if(std::isnan(p_.x())||std::isnan(p_.y())||std::isnan(p_.z()))
  	    {
  	        ROS_ERROR("NaNs");
  	    }
  	    else
  	    {
  	        points[i].x = p_.x(); points[i].y = p_.y(); points[i].z = p_.z();
        }
  	    
  	}
      	
}


void PointCloudInterpolator::transformPoint4Vector(std::vector<Vector4D> &points,tf::Transform &transform)
{
    Vector3D p;
  	p.x() = transform.getOrigin().x(); p.y() = transform.getOrigin().y(); p.z() = transform.getOrigin().z();
  	Eigen::Quaterniond q;
  	q.x() = transform.getRotation().x(); q.y() = transform.getRotation().y(); q.z() = transform.getRotation().z(); q.w() = transform.getRotation().w();

  	Vector3D p_;

  	for(unsigned int i=0;i<points.size();i++)
  	{
  	    p_.x()= points[i][0] ;p_.y()= points[i][1] ;p_.z()= points[i][2];
  	    p_ = q*(p_) + p;
  	    if(std::isnan(p_.x())||std::isnan(p_.y())||std::isnan(p_.z()))
  	    {
  	        ROS_ERROR("NaNs");
  	    }
  	    else
  	    {
  	        points[i][0] = p_.x(); points[i][1] = p_.y(); points[i][2] = p_.z();
        }

  	}

}


int getFieldOffset(sensor_msgs::PointCloud2 &scan,std::string intensityChannelName,bool& float32)
{
    int offset = -1;
    
    for(unsigned int i= 0;i<scan.fields.size();i++)
    {
        if(scan.fields[i].name == intensityChannelName)
        {
          offset = scan.fields[i].offset;
          if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT32)
            float32 = true;
          else if(scan.fields[i].datatype == sensor_msgs::PointField::FLOAT64)
            float32 = false;
          else
            {
              ROS_ERROR_STREAM("Field was of wrong type");
              return offset;
            }
            
            break;
        }
    }
    
    return offset;
        
}

std::vector<double> PointCloudInterpolator::getIntensityfromPointCloud2(sensor_msgs::PointCloud2 &scan,std::string intensityChannelName)
{

    std::vector<double>  intensity;
    bool float32;
    int offset = getFieldOffset(scan,intensityChannelName,float32);
    
    if(offset<0)
    {
        ROS_ERROR("Cant find intensity channel");
        return intensity;
    }
    
    unsigned int num_points = scan.height*scan.width;
    void *arr  = calloc(1,8);
    double value;
       
    for(unsigned int i=0; i<num_points;i++)
    {
        if(float32)
        {
            memcpy (arr,&scan.data[i*scan.point_step + offset],4);
            float value_p = *((float*) arr);
            value = (double) value_p;
        }
        else
        {
            memcpy (arr,&scan.data[i*scan.point_step + offset],8);
            double value_p = *((double*) arr);
            value = value_p;
        }
        
        intensity.push_back(value);
        
    }
    free(arr);
    return intensity;

};


