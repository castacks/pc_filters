/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>

#include <pcl/io/pcd_io.h>

#include <pcl/features/range_image_border_extractor.h>
#include <pcl/range_image/range_image_planar.h>

#include <sensor_msgs/Image.h>
#include <stereo_msgs/DisparityImage.h>
#include <sensor_msgs/CameraInfo.h>

using namespace std;
using namespace pcl;
typedef PointXYZ PointType;

ros::Publisher border_pub, veil_pub, shadow_pub;
double leaf_size,minrange,maxrange;
float angular_resolution = 0.1f;  // Resolution of the range image

void
image_msg_cb (const stereo_msgs::DisparityImageConstPtr& image_msg)
{

	  RangeImagePlanar* range_image_planar=NULL;
	  RangeImage* range_image_ptr;

	  range_image_planar = new RangeImagePlanar;
	  range_image_ptr = range_image_planar;

	  RangeImage& range_image = *range_image_ptr;

	  range_image_planar->setDisparityImage (reinterpret_cast<const float*> (&image_msg->image.data[0]),
			image_msg->image.width, image_msg->image.height,
			image_msg->f, image_msg->T, angular_resolution);

	range_image.setUnseenToMaxRange ();


	RangeImageBorderExtractor border_extractor (&range_image);
	PointCloud<BorderDescription> border_descriptions;

	border_extractor.compute (border_descriptions);

	pcl::PointCloud<PointWithRange> border_points, veil_points, shadow_points;
	int index = 0;
	for (int y=0; y< (int)range_image.height; ++y)
	{
		for (int x=0; x< (int)range_image.width; ++x)
		{
			index = y*range_image.width + x;
			if (border_descriptions.points[index].traits[BORDER_TRAIT__OBSTACLE_BORDER])
				border_points.points.push_back (range_image.points[index]);
			else if (border_descriptions.points[index].traits[BORDER_TRAIT__VEIL_POINT])
				veil_points.points.push_back (range_image.points[index]);
			else if (border_descriptions.points[index].traits[BORDER_TRAIT__SHADOW_BORDER])
				shadow_points.points.push_back (range_image.points[index]);
		}
	}

	border_points.header= image_msg->header;
	veil_points.header= image_msg->header;
	shadow_points.header= image_msg->header;

	border_pub.publish(border_points);
	veil_pub.publish(veil_points);
	shadow_pub.publish(shadow_points);


}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "filter_disparity");
	ros::NodeHandle nh;
	ros::NodeHandle np("~");

	np.param<double>("minrange", minrange, 	0.0);
	np.param<double>("maxrange", maxrange, 	20.0);
	np.param<double>("leaf_size", leaf_size, 	0.5);
	ros::Subscriber sub = nh.subscribe("/ueye/disparity", 1, image_msg_cb);
	border_pub = nh.advertise<sensor_msgs::PointCloud2>("border_points", 1);
	veil_pub = nh.advertise<sensor_msgs::PointCloud2>("veil_points", 1);
	shadow_pub = nh.advertise<sensor_msgs::PointCloud2>("shadow_points", 1);

	ros::spin();
	return 0;
}

