/*
* Copyright (c) 2016 Carnegie Mellon University, Author <achamber>
*
* For License information please see the LICENSE file in the root directory.
*
*/
/*


 * filter_cloud_by_agl.cpp
 *
 *  Created on: May 21, 2013
 *      Author: achamber
 */

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>

#include <pcl/point_cloud.h>

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "std_msgs/Float64.h"
#include <nav_msgs/Odometry.h>
#include <pcl/filters/crop_box.h>

using namespace std;

class AglCropBox
{

private:
	double max_height;
	double ground_height;
	double vx, vy, vz;
	string reference_frame;
	ros::Publisher cloud_pub;
	ros::Subscriber cloud_sub, agl_sub, odom_sub;
	bool agl_valid, odom_valid;

public:

	AglCropBox ()
	{
		ros::NodeHandle n;
		ros::NodeHandle np("~");

		np.param<double>("max_height", max_height, 1);
		ground_height = 0;
		vx = 0;
		vy = 0;
		vz = 0;
		agl_valid = false;
		odom_valid = false;

		cloud_sub = n.subscribe("input", 10, &AglCropBox::cloudCallback, this);
		agl_sub = n.subscribe("agl", 10, &AglCropBox::aglCallback, this);
		odom_sub = n.subscribe("odom", 10, &AglCropBox::odomCallback, this);

		cloud_pub = n.advertise<sensor_msgs::PointCloud2>("output",10);
	}

	void cloudCallback(const sensor_msgs::PointCloud2::ConstPtr& msg)
	{

		if (!agl_valid || !odom_valid)
			return;

		if (reference_frame.empty())
			reference_frame = msg->header.frame_id;


		pcl::PointCloud<pcl::PointXYZ> cloud, cloud_filtered;
		pcl::fromROSMsg(*msg, cloud);
		//						cropCloud(pcd, pcd_filtered);
		//						scan_pub_.publish(pcd_filtered);


		int count = 0;
		for (int i = 0; i < cloud.points.size(); i++)
		{

			if (cloud.points[i].z < ground_height-max_height)
			{
				cloud_filtered.push_back(cloud.points[i]);
				count++;
			}
		}

		cloud_filtered.header = cloud.header;
		cloud_filtered.width = count;

		cloud_pub.publish(cloud_filtered);
	}

	void aglCallback(const std_msgs::Float64::ConstPtr& msg)
	{

		agl_valid = true;

		ground_height = vz + msg->data;
		//ROS_INFO_STREAM("ground_height: " << ground_height);

	}

	void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
	{

		odom_valid = true;

		vx = msg->pose.pose.position.x;
		vy = msg->pose.pose.position.y;
		vz = msg->pose.pose.position.z;

		// TODO Deal with case when laser points and vehicle odometry are in different frames
		if (!reference_frame.empty() && reference_frame != msg->header.frame_id)
		{
			ROS_ERROR_STREAM("The reference frame of the point cloud does not match the frame of the vehicle. Filtering by agl will NOT work!");
			ROS_ERROR_STREAM( reference_frame << " != " << msg->header.frame_id);

		}
	}

};


int main(int argc, char** argv)
{
	ros::init(argc, argv, "agl_box_filter", ros::init_options::AnonymousName);

	AglCropBox b;

	ros::spin();
	return(0);
}

