/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>

ros::Publisher pub;
double leaf_size,minrange,maxrange;

void callback(const pcl::PCLPointCloud2ConstPtr& cloud)
{


  pcl::PCLPointCloud2Ptr cloud_cropped (new pcl::PCLPointCloud2);

  pcl::PassThrough<pcl::PCLPointCloud2> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (minrange, maxrange);
  pass.filter (*cloud_cropped);

  pcl::PCLPointCloud2 cloud_filtered;

  // Perform the actual filtering
  pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
  sor.setInputCloud (cloud_cropped);
  sor.setDownsampleAllData(true);
  sor.setLeafSize (leaf_size, leaf_size, leaf_size);
  sor.filter (cloud_filtered);

  // Publish the data
  pub.publish (cloud_filtered);


}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "filter_pcd");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  np.param<double>("minrange", minrange, 	0.0);
  np.param<double>("maxrange", maxrange, 	20.0);
  np.param<double>("leaf_size", leaf_size, 	0.5);
  ros::Subscriber sub = nh.subscribe<pcl::PCLPointCloud2>("input", 2, callback);
  pub = nh.advertise<sensor_msgs::PointCloud2>("output", 2);
  ros::spin();
  return 0;
}

