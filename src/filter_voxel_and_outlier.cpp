/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <boost/foreach.hpp>
#include "pcl/io/pcd_io.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/filters/voxel_grid.h>
#include "pcl/filters/statistical_outlier_removal.h"

void callback(const pcl::PCLPointCloud2ConstPtr& msg)
{
  //printf ("Cloud: width = %d, height = %d\n", msg->width, msg->height);
  //BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
    //printf ("\t(%f, %f, %f)\n", pt.x, pt.y, pt.z);

  ROS_INFO ("PointCloud before filtering: %d data points (%s).", msg->width * msg->height, pcl::getFieldsList (*msg).c_str ());

  // Create the filtering object
  pcl::VoxelGrid<pcl::PCLPointCloud2> voxsor;
  voxsor.setInputCloud (msg);
  voxsor.setLeafSize (0.001, 0.001, 0.001);
  pcl::PCLPointCloud2Ptr inlier_cloud (new pcl::PCLPointCloud2);
  voxsor.filter (*inlier_cloud);

  ROS_INFO ("PointCloud after voxel filtering: %d data points (%s).", inlier_cloud->width * inlier_cloud->height, pcl::getFieldsList (*inlier_cloud).c_str ());

  // Create the filtering object
  pcl::StatisticalOutlierRemoval<pcl::PCLPointCloud2> inliersor;
  inliersor.setInputCloud (inlier_cloud);
  inliersor.setMeanK (50);
  inliersor.setStddevMulThresh (1.0);
  pcl::PCLPointCloud2Ptr cloud_filtered (new pcl::PCLPointCloud2);
  inliersor.filter (*cloud_filtered);

  ROS_INFO ("PointCloud after outlier filtering: %d data points (%s).", cloud_filtered->width * cloud_filtered->height, pcl::getFieldsList (*inlier_cloud).c_str ());

  //std::cerr << "Cloud after filtering: " << std::endl;
  //std::cerr << *cloud_filtered << std::endl;


}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "filter_pcd");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe("points2", 1, callback);
  ros::spin();
  return 0;
}

