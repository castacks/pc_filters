/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <dynamic_reconfigure/server.h>
#include <pc_filters/FilterConfig.h>

ros::Publisher pub;
double leaf_size,minrange,maxrange;

int meanK_ = 10;
double stddevMult_ = 1.0;

void callback(pc_filters::FilterConfig &config, uint32_t level)
{

	meanK_ = config.knn;
	stddevMult_ = config.stddev_mult;
/*
  ROS_INFO("Reconfigure request : %f %f %i %i %i %s %i %s %f %i",
           config.groups.angles.min_ang, config.groups.angles.max_ang,
           (int)config.intensity, config.cluster, config.skip,
           config.port.c_str(),(int)config.calibrate_time,
           config.frame_id.c_str(), config.time_offset,
           (int)config.allow_unsafe_settings);
*/

  // do nothing for now
}


void callback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& cloud)
{
	
	
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cropped (new pcl::PointCloud<pcl::PointXYZ>);

  pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (minrange, maxrange);
  pass.filter (*cloud_cropped);

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

  // Perform the actual filtering
  pcl::ApproximateVoxelGrid<pcl::PointXYZ> sor;
  sor.setInputCloud (cloud_cropped);
  sor.setDownsampleAllData(true);
  sor.setLeafSize (leaf_size, leaf_size, leaf_size);
  sor.filter (*cloud_filtered);


  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_inliers (new pcl::PointCloud<pcl::PointXYZ>);
  
// Create the filtering object
  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> inliersor;
  inliersor.setInputCloud (cloud_filtered);
  inliersor.setMeanK (meanK_);
  inliersor.setStddevMulThresh (stddevMult_);
  inliersor.filter (*cloud_inliers);
  

  // Publish the data
  pub.publish (*cloud_inliers);


}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "filter_pcd");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  ros::init(argc, argv, "dynamic_reconfigure_node");
  dynamic_reconfigure::Server<pc_filters::FilterConfig> srv;
  dynamic_reconfigure::Server<pc_filters::FilterConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  srv.setCallback(f);

  np.param<double>("minrange", minrange, 	0.0);
  np.param<double>("maxrange", maxrange, 	20.0);
  np.param<double>("leaf_size", leaf_size, 	0.5);
  np.param<int>("meank", meanK_, 	5);
  np.param<double>("stddevMult", stddevMult_, 	0.001);
  ros::Subscriber sub = nh.subscribe< pcl::PointCloud<pcl::PointXYZ> >("input", 2, callback);
  pub = nh.advertise<sensor_msgs::PointCloud2>("output", 2);




  ros::spin();
  return 0;
}

