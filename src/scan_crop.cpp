/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <algorithm>

ros::Publisher pub;
ros::Publisher pub2;

void callback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
	sensor_msgs::LaserScan scan = (*msg);
	double totalAngle = scan.angle_max - scan.angle_min;
	double removeAngle = (totalAngle - 2.3)/2;
	double removeNum = round(removeAngle/scan.angle_increment);

	for(unsigned int it=0; it<scan.ranges.size(); it++)
	{
		if(it < removeNum || it > scan.ranges.size() - removeNum || scan.ranges[it] < 0.5)
		{
			scan.intensities[it] = 0;
			scan.ranges[it] = 0;
			continue;
		}
	}
	pub.publish (scan);
}

void callback2(const sensor_msgs::LaserScan::ConstPtr& msg)
{
	sensor_msgs::LaserScan scan = (*msg);
	double totalAngle = scan.angle_max - scan.angle_min;
	double removeAngle = (totalAngle - 2.3)/2;
	double removeNum = round(removeAngle/scan.angle_increment);

	for(unsigned int it=0; it<scan.ranges.size(); it++)
	{
		if(it < removeNum || it > scan.ranges.size() - removeNum || scan.ranges[it] < 0.5)
		{
			scan.intensities[it] = 0;
			scan.ranges[it] = 0;
			continue;
		}
	}
	pub2.publish (scan);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "scan_crop");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe<sensor_msgs::LaserScan>("/primary_scan", 2, callback);
  ros::Subscriber sub2 = nh.subscribe<sensor_msgs::LaserScan>("/secondary_scan", 2, callback2);
  pub = nh.advertise<sensor_msgs::LaserScan>("/scan_crop/output", 2);
  pub2 = nh.advertise<sensor_msgs::LaserScan>("/scan_crop/output2", 2);
  ros::spin();
  return 0;
}

