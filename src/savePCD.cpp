/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <pcl_ros/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/io.h>
#include <pcl/point_types.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <dynamic_reconfigure/server.h>
#include <pc_filters/FilterConfig.h>


ros::Publisher pub;
double leaf_size,minrange,maxrange;

int meanK_ = 10;
double stddevMult_ = 1.0;
bool invertIntensity;

void callback(pc_filters::FilterConfig &config, uint32_t level)
{

	meanK_ = config.knn;
	stddevMult_ = config.stddev_mult;
/*
  ROS_INFO("Reconfigure request : %f %f %i %i %i %s %i %s %f %i",
           config.groups.angles.min_ang, config.groups.angles.max_ang,
           (int)config.intensity, config.cluster, config.skip,
           config.port.c_str(),(int)config.calibrate_time,
           config.frame_id.c_str(), config.time_offset,
           (int)config.allow_unsafe_settings);
*/

  // do nothing for now
}

void callback(const sensor_msgs::PointCloud2::ConstPtr& cloud)
{
 sensor_msgs::PointCloud2::Ptr cloud_cropped (new sensor_msgs::PointCloud2);

  pcl::PassThrough<sensor_msgs::PointCloud2> ptfilter (false); // Initializing with true will allow us to extract the removed indices
  ptfilter.setInputCloud (cloud);
  ptfilter.setFilterFieldName ("z");
  ptfilter.setFilterLimits (minrange, maxrange);
  ptfilter.filter (*cloud_cropped);
 

  //sensor_msgs::PointCloud2::Ptr cloud_cropped (new sensor_msgs::PointCloud2);
 pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_filtered_pcl (new pcl::PointCloud<pcl::PointXYZI>);
 pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_cropped_pcl (new pcl::PointCloud<pcl::PointXYZI>);
  pcl::fromROSMsg(*cloud_cropped,*cloud_cropped_pcl);  

	//Intensity
	if(invertIntensity)
	{
		for (size_t i = 0; i < cloud_cropped_pcl->points.size(); i++) {
			cloud_cropped_pcl->points[i].intensity = 255-cloud_cropped_pcl->points[i].intensity;
			//ROS_ERROR_STREAM("cloud_filtered_pcl->points[i].intensity "<<cloud_filtered_pcl->points[i].intensity);
		}
	}	
  // Perform the actual filtering
 // pcl::ApproximateVoxelGrid<pcl::PointXYZI> sor;
  pcl::VoxelGrid<pcl::PointXYZI> sor;
  sor.setInputCloud (cloud_cropped_pcl);
  sor.setDownsampleAllData(true);
  sor.setLeafSize (leaf_size, leaf_size, leaf_size);
  sor.filter (*cloud_filtered_pcl);
 /*
	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_inliers_pcl (new pcl::PointCloud<pcl::PointXYZI>);
   // Create the filtering object
  pcl::StatisticalOutlierRemoval<pcl::PointXYZI> inliersor;
  inliersor.setInputCloud (cloud_filtered_pcl);
  inliersor.setMeanK (meanK_);
  inliersor.setStddevMulThresh (stddevMult_);
  inliersor.filter (*cloud_inliers_pcl);
*/
  sensor_msgs::PointCloud2::Ptr cloud_filtered_pub (new sensor_msgs::PointCloud2);

  pcl::toROSMsg (*cloud_filtered_pcl,*cloud_filtered_pub);
  pub.publish (*cloud_filtered_pub);
   
  // Publish the data
  //pub.publish (*cloud_filtered);


}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "save_pcd_from_pc");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  ros::init(argc, argv, "dynamic_reconfigure_node");
  dynamic_reconfigure::Server<pc_filters::FilterConfig> srv;
  dynamic_reconfigure::Server<pc_filters::FilterConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  srv.setCallback(f);

  np.param<double>("minrange", minrange, 	0);
  np.param<double>("maxrange", maxrange, 	20);
  np.param<double>("leaf_size", leaf_size, 	0.5);
  np.param<bool>("invertIntensity", invertIntensity, 	true);
  np.param<int>("meank", meanK_, 	5);
  np.param<double>("stddevMult", stddevMult_, 	0.001);
  ros::Subscriber sub = nh.subscribe<sensor_msgs::PointCloud2>("input", 2, callback);
  pub = nh.advertise<sensor_msgs::PointCloud2>("output", 2);
 // save_pcd_map_service_ = nh.advertiseService("save_pcd_map", &savePcdMapSrvCallback, this);


  ros::spin();
  return 0;
}


