#ifndef _POINTCOULD_INTERPOLATOR_
#define _POINTCOULD_INTERPOLATOR_

#include <tf/tf.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Point.h>
#include <ca_common/math.h>
namespace CA
{
   typedef geometry_msgs::Point PointD;

   struct OffsetStruct
   {
      int xOff;
      bool  xFloat;
      int yOff;
      bool yFloat;
      int zOff;
      bool zFloat;
      int tOff;
      bool tFloat;
      int iOff;
      bool iFloat;
   };

   struct TimeVector
   {
     double time;
     tf::Vector3 point;
   }; 
    
        
  class PointCloudInterpolator
  {
  public:
    PointCloudInterpolator(){}
    ~PointCloudInterpolator(){}
    bool transform(const std::string &targetFrame,  sensor_msgs::PointCloud2& scan, tf::Transformer &tf, int tfLookupincrement);
    std::vector<PointD> getPointsfromPointCloud2(sensor_msgs::PointCloud2 &scan);
    std::vector<Vector4D> getPoints4fromPointCloud2(sensor_msgs::PointCloud2 &scan);
    std::vector<double> getIntensityfromPointCloud2(sensor_msgs::PointCloud2 &scan,std::string intensityChannelName);
    void transformPointDVector(std::vector<PointD> &points,tf::Transform &transform);
    void transformPointVector(std::vector<geometry_msgs::Point> &points,tf::Transform &transform);
    void transformPoint4Vector(std::vector<Vector4D> &points,tf::Transform &transform);
    OffsetStruct getXYZIoffset(sensor_msgs::PointCloud2 &scan);
    OffsetStruct getXYZoffset(sensor_msgs::PointCloud2 &scan);
  };

}

#endif
